/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python, Java, PHP, Ruby, Perl,
C#, VB, Swift, Pascal, Fortran, Haskell, Objective-C, Assembly, HTML, CSS, JS, SQLite, Prolog.
Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/
#include <stdio.h>
#include <iostream>
using namespace std;

int main()
{
    // Hold modular operations for 3 and 5
    int mod3 = 0;               
    int mod5 = 0;
    // Loop through numbers
    for(int i=1;i<=100;i++) 
    {
        // mod3 is non-zero if number is multiple of 3
        mod3 = !(i%3);
        // mod5 is non-zero if number is multiple of 5
        mod5 = !(i%5);
        
        // If multiple os 3 or 5
        if(mod3 || mod5)
        {
            // For i%3 == 0 print "Foo";
            if(mod3)
                cout << "Foo";
            // For i%5 == 0 print "Baa";
            if(mod5)
                cout << "Baa";
        }
        // Not a multiple
        else
        {
            // Print number
            cout << i;
        }
        // Always print new line
        cout << endl;
    }

    // Return 0 to be safe 
    return 0;
}

