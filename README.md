## Question 3 for Siemens Programming skills and PL/SQL

This repository holds code for the third question of Siemens Programming skills and PL/SQL.

The code consists of a program that prints each number from 1 to 100 on a new line.

For each multiple of 3, print "Foo" instead of the number.
For each multiple of 5, print "Baa" instead of the number.
For multiple numbers simultaneously of 3 and 5, print "FooBaa", instead of the number.
